
use std::io;


struct Disc {
    number: i32,
}

impl Disc {
    fn new(number: i32) -> Disc {
        Disc {
            number: number,
        }
    }
}

struct Tower {
    id: i32,
    element: Vec<Disc>,
}

fn main() {
    println!("Tower of Hanoi!");
    let d1 = Disc::new(1);
    let d2 = Disc::new(2);
    let d3 = Disc::new(3);

    let mut t1 = Tower {
        id: 1,
        element: vec![],
    };
    let mut t2 = Tower {
        id: 2,
        element: vec![],
    };
    let mut t3 = Tower {
        id: 3,
        element: vec![],
    };

    t1.element.push(d3);
    t1.element.push(d2);
    t1.element.push(d1);
    
    let mut tower_ary = [t1, t2, t3];

    println!("1番の塔を選択するとき：1キーを押してEnter");
    println!("2番の塔を選択するとき：2キーを押してEnter");
    println!("3番の塔を選択するとき：3キーを押してEnter");

    loop {
        println!("現在のディスクの状態を表示します");
        for i in tower_ary.iter() {
            print!("{} |", i.id);
            for j in i.element.iter() {
                print!("{} ", j.number);
            }
            println!("");
        }

        if tower_ary[1].element.len() == tower_ary.len() || tower_ary[2].element.len() == tower_ary.len() {
            println!("Clear!!");
            break;
        }

        println!("どうする？：");
        

        let mut select = String::new();

        let select_disc;

        io::stdin().read_line(&mut select)
                .expect("Failed to read line");

        let select: u32 = match select.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        if select > 0 && select < 4 {
            if tower_ary[select as usize - 1].element.len() > 0 {
                select_disc = tower_ary[select as usize - 1].element.pop().unwrap();
            } else {
                println!("その塔にはなにもありません。");
                continue;
            }
        } else {
            println!("1～3の数字を選択してください。");
            continue;
        }

        println!("{} のディスクをどこに移動させる？", select_disc.number);

        let mut move_number = String::new();

        io::stdin().read_line(&mut move_number)
                .expect("Failed to read line");

        let move_number: u32 = match move_number.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        if move_number > 0 && move_number < 4 {
            if tower_ary[move_number as usize - 1].element.len() == 0 || tower_ary[move_number as usize - 1].element.last().unwrap().number > select_disc.number {
                tower_ary[move_number as usize - 1].element.push(select_disc);
            } else {
                println!("大きいディスクを小さいディスクの上には置けません。");
                tower_ary[select as usize - 1].element.push(select_disc);
            }
        } else {
            println!("1～3の数字を選択してください。");
            continue;
        }
    }
}
